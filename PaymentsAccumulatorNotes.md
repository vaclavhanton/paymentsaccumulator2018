# Notes #

## Known issues ##
* Regular balance outputs are mixing together with system.in, better way to handle would be use some simple console UI.
* Using Spring is not necessary for such simple application, until further development is expected.

### i18n ###
* There is many reasons to have all strings presented to user stored centrally e.g.: i18n, label generation using parameters.
* Currently error messages, help etc. are hardcoded in *PAStringProvider*, static util class, as a preparation of usage of centralized string provider service.
* Due to need fill messages with dynamic content, best would be use some standardized i18n approach.

### PaymentsAccumulator ###
* Reading from a file and System.in loops are separated for easier handling or error messages. Since both uses BufferedReader and input is in same format, reading can be collapsed into one loop, what would be more elegant. 

### Payments validation and parse ###
* Format validation fails after first error is found, intent is not to find all errors in one definition, but recognize wrong input line (for file), format is also too easy to be able to recognize multiple errors on one line
* If fails:
	* Should be logged - instead System.err is used
	* Throws exception - to make caller handle the wrong input
* If validation and parse is correct:
	* No error wrote
	* new Payment object is returned

### Payments ###
* Payments can be negative and also positive - SHOULD BE DISCUSSED WITH SPECIFIER, payment direction(accounts reference, etc.) would be better

### Currency ###
* There is explicit requirement that currency can be any uppercase 3-letter code, so we cannot use java.util.Currency - SHOULD BE DISCUSSED WITH SPECIFIER, propose to use standard currency

### Net Amoounts - currency balance ###
* Find proper way to write to System.out and read from System.in is tricky, better way to handle it would be use some simple console UI
* Balance is calculated dynamically during adding the payments, it is not calculated on each balance query

### Validator ###
* In case that payments are loaded to database (represented by *PAPaymentDatabaseRepositoryImpl*) not only using PaymentAccumulator, there is available balance validator, and method to recalculate the balance if not valid. Currently it runs each 5 seconds.

### Test ###
There is test of one model and one service classes. Rather than complete unit tests, those are examples how such classes can be tested. Tests:
* PACurrencyTest
* PACurrencyConverterServiceImplTest