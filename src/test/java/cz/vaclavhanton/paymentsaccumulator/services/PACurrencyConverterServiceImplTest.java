package cz.vaclavhanton.paymentsaccumulator.services;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrencyRate;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyDatabaseRepository;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyDatabaseRepositoryImpl;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyRateDatabase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PACurrencyConverterServiceImplTest {

    private static double fixedRate = 1.123456;
    private static double originalAmount = 300;

    @TestConfiguration
    static class PACurrencyConvertorServiceImplTestConfiguration {

        @Bean
        PACurencyConverterService curencyConverterService() {
            return new PACurencyConverterServiceImpl();
        }

        @Bean
        PACurrencyDatabaseRepository currencyDatabaseRepository() {
            return new PACurrencyDatabaseRepositoryImpl();
        }

        @Bean
        PACurrencyRateDatabase currencyRateDatabase() {
            return new PACurrencyRateDatabase() {
                @Override
                public PACurrencyRate getDummyRate(PACurrency a, PACurrency b) throws WrongArgumentException {
                    return new PACurrencyRate(a, b, fixedRate);
                }
            };
        }
    }

    @Autowired
    @Qualifier("curencyConverterService")
    private PACurencyConverterService curencyConverterService;

    @Autowired
    @Qualifier("currencyDatabaseRepository")
    private PACurrencyDatabaseRepository currencyDatabaseRepository;

    @Test
    public void testConversion() throws Throwable {

        PAPayment convertedPayment = curencyConverterService.getConvertedPayment(//
                new PAPayment(currencyDatabaseRepository.getCurrencyByName("EUR"), originalAmount), currencyDatabaseRepository.getCurrencyByName("USD"));

        Assert.assertNotNull(convertedPayment);
        Assert.assertNotNull(convertedPayment.getCurrency());
        Assert.assertNotNull(convertedPayment.getAmount());

        Assert.assertEquals("PACurrency{code='USD'}", convertedPayment.getCurrency().toString());
        Assert.assertEquals(originalAmount * fixedRate, convertedPayment.getAmount(), 0);
    }
}
