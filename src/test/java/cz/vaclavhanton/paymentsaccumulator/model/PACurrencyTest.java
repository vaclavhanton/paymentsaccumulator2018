package cz.vaclavhanton.paymentsaccumulator.model;

import cz.vaclavhanton.paymentsaccumulator.exceptions.CurrencyNotParsableException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by vaclav.hanton on 9.6.2016.
 */
public class PACurrencyTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCurrencyFormatWithConstructorSuccess() {
        PACurrency curr = null;

        String okCurrency1 = "HHH";
        try {
            curr = new PACurrency(okCurrency1);
        } catch (CurrencyNotParsableException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(curr);
        Assert.assertEquals(okCurrency1, curr.getCode());
    }

    @Test
    public void testCurrencyFormatWithConstructorWrongNumerical() throws CurrencyNotParsableException {

        thrown.expect(CurrencyNotParsableException.class);
        thrown.expectMessage("Cannot parse currency code: H45");

        String okCurrency1 = "H45";
        new PACurrency(okCurrency1);
    }

    @Test
    public void testCurrencyFormatWithConstructorWrongNonUpperCase() throws CurrencyNotParsableException {
        thrown.expect(CurrencyNotParsableException.class);
        thrown.expectMessage("Cannot parse currency code: BAc");

        String okCurrency1 = "BAc";
        new PACurrency(okCurrency1);
    }

    @Test
    public void testCurrencyFormatWithConstructorWrongLenght() throws CurrencyNotParsableException {

        thrown.expect(CurrencyNotParsableException.class);
        thrown.expectMessage("Cannot parse currency code: AAAA");

        String okCurrency1 = "AAAA";
        new PACurrency(okCurrency1);
    }
}