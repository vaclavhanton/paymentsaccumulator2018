package cz.vaclavhanton.paymentsaccumulator.model;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;

/**
 * Created by vaclav.hanton on 13.6.2016.
 */
public class PACurrencyRate {

    private PACurrency a;

    private PACurrency b;

    double rate;

    /**
     * Creates rate object, for non same currencies
     *
     * @param a    currency a
     * @param b    currency b
     * @param rate rate
     * @throws WrongArgumentException - probably a.equals(b)
     */
    public PACurrencyRate(PACurrency a, PACurrency b, double rate) throws WrongArgumentException {

        if (a.equals(b)) {
            throw new WrongArgumentException(PAStringProvider.CANNOT_CREATE_RATE_EQUAL_CURRENCIES);
        }

        this.a = a;
        this.b = b;
        this.rate = rate;
    }

    public PACurrency getA() {
        return a;
    }

    public PACurrency getB() {
        return b;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PACurrencyRate that = (PACurrencyRate) o;

        if (Double.compare(that.rate, rate) != 0) return false;
        if (!a.equals(that.a)) return false;
        return b.equals(that.b);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = a.hashCode();
        result = 31 * result + b.hashCode();
        temp = Double.doubleToLongBits(rate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
