package cz.vaclavhanton.paymentsaccumulator.model;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
public class PAPayment {

    PACurrency currency;
    double amount;

    /**
     * Creates PAPayment objecy
     *
     * @param currency
     * @param amount
     * @throws WrongArgumentException
     */
    public PAPayment(PACurrency currency, double amount) throws WrongArgumentException {
        if (currency == null) {
            throw new WrongArgumentException(PAStringProvider.CURRENCY_NULL);
        }

        this.currency = currency;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public PACurrency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "PAPayment{" +
                "currency=" + currency +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PAPayment payment = (PAPayment) o;

        if (Double.compare(payment.amount, amount) != 0) return false;
        return currency.equals(payment.currency);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = currency.hashCode();
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
