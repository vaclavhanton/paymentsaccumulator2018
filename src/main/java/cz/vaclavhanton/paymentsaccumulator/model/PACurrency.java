package cz.vaclavhanton.paymentsaccumulator.model;

import cz.vaclavhanton.paymentsaccumulator.exceptions.CurrencyNotParsableException;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
public class PACurrency {

    String code;

    /**
     * If input is 3 uppercase alpha chars creates currency object
     *
     * @param code - 3 uppercase alpha chars
     * @throws CurrencyNotParsableException - wrong code format
     */
    public PACurrency(String code) throws CurrencyNotParsableException {

        if (code != null && code.length() == 3 && StringUtils.isAllUpperCase(code)) {
            this.code = code;
            return;
        }

        throw new CurrencyNotParsableException(PAStringProvider.CANNOT_PARSE_CURRENCY_CODE + ": " + code);
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "PACurrency{" +
                "code='" + code + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PACurrency that = (PACurrency) o;

        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
