package cz.vaclavhanton.paymentsaccumulator.services;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrencyRate;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyRateDatabase;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by vaclav.hanton on 13.6.2016.
 */
@Service("curencyConverterService")
public class PACurencyConverterServiceImpl implements PACurencyConverterService {

    @Autowired
    @Qualifier("currencyRateDatabase")
    PACurrencyRateDatabase paCurrencyRateDatabase;

    /**
     * Returns temporary(not saved to repositories) payment object, that holds amount recalculated
     * to target currency using rate from repositories
     *
     * @param p
     * @param targetCurrency
     * @return
     * @throws WrongArgumentException
     */
    @Override
    public PAPayment getConvertedPayment(PAPayment p, PACurrency targetCurrency)
            throws WrongArgumentException {

        if (p == null || targetCurrency == null) {
            throw new WrongArgumentException(PAStringProvider.EMPTY_ARGUMENTS_FOR_CONVERTING_PAYMENT);
        }

        PACurrencyRate targetRate = paCurrencyRateDatabase.getDummyRate(p.getCurrency(), targetCurrency);

        if (targetRate == null) {
            return null;
        }

        return new PAPayment(targetCurrency, p.getAmount() * targetRate.getRate());
    }
}
