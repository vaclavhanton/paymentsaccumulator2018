package cz.vaclavhanton.paymentsaccumulator.services;

import cz.vaclavhanton.paymentsaccumulator.exceptions.PAException;

import java.io.FileReader;
import java.io.IOException;

public interface PAPaymentsAccumulatorService {

    /**
     * Reads file of formatted payments. Payments are created and saved.
     * When finish reading, writes down list of errors
     * including line numbers in source file and specific error for each line
     *
     * @param fileReader
     * @return
     * @throws Exception - PAExceptions are caught and represented as format errors,
     *                   others thrown
     */
    int loadPaymentsFromFile(FileReader fileReader) throws IOException, PAException;

    /**
     * Starts System.in reading. Creates payments, and sawing them to repositories,
     * or writes format error if payment reading fails.
     *
     * @throws Exception - PAExceptions are caught and represented as format errors,
     *                   others thrown
     */
    void runCommandLineInterface() throws IOException, PAException;
}
