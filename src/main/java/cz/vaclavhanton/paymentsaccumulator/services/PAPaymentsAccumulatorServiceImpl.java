package cz.vaclavhanton.paymentsaccumulator.services;

import cz.vaclavhanton.paymentsaccumulator.RuntimeUtil;
import cz.vaclavhanton.paymentsaccumulator.exceptions.PAException;
import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongPaymentFileException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyDatabaseRepository;
import cz.vaclavhanton.paymentsaccumulator.repositories.PACurrencyRateDatabase;
import cz.vaclavhanton.paymentsaccumulator.repositories.PAPaymentDatabaseRepository;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
@Service("paymentsAccumulator")
public class PAPaymentsAccumulatorServiceImpl implements PAPaymentsAccumulatorService {

    @Autowired
    @Qualifier("currencyDatabaseRepository")
    public PACurrencyDatabaseRepository paCurrencyDatabase;

    @Autowired
    @Qualifier("currencyRateDatabase")
    public PACurrencyRateDatabase paCurrencyRateDatabase;

    @Autowired
    @Qualifier("paymentDatabaseRepository")
    PAPaymentDatabaseRepository paymentDatabaseRepository;

    @Autowired
    @Qualifier("curencyConverterService")
    PACurencyConverterService paCurencyConverterService;

    @Autowired
    RuntimeUtil runtimeUtil;

    /**
     * Reads file of formatted payments. Payments are created and saved.
     * When finish reading, writes down list of errors
     * including line numbers in source file and specific error for each line
     *
     * @param fileReader
     * @return
     * @throws Exception - PAExceptions are caught and represented as format errors,
     *                   others thrown
     */
    @Override
    public int loadPaymentsFromFile(FileReader fileReader) throws IOException, PAException {

        if (fileReader == null)
            throw new WrongPaymentFileException(PAStringProvider.NULL_FILE_READER_PASSED);

        BufferedReader br = new BufferedReader(fileReader);

        int createdPayments = 0;
        int lineNumber = 0;
        String line;

        Map<Integer, String> errorMessages = new HashMap<>();

        while ((line = br.readLine()) != null) {

            // Skip empty lines
            if (line.isEmpty()) {
                continue;
            }

            PAPayment sucessfullPayment = null;

            lineNumber++;

            try {
                sucessfullPayment = validateAndCreatePayment(line);
            } catch (NumberFormatException nfe) {
                errorMessages.put(lineNumber, PAStringProvider.INCORRECT_FORMAT_OF_AMOUNT_NUMBER + ": " + nfe.getMessage());
                continue;
            } catch (PAException pae) {
                String message = String.format(
                        PAStringProvider.COULD_NOT_PARSE_PAYMENT_FROM + ": %s, " + PAStringProvider.ERROR_MESSAGE +
                                ": %s", line, pae.getMessage());
                errorMessages.put(lineNumber, message);
                continue;
            }

            PAPayment convertedPayment = paCurencyConverterService
                    .getConvertedPayment(sucessfullPayment, paCurrencyDatabase.getCurrencyByName(PAStringProvider.USD));
            StringBuilder buf = new StringBuilder(PAStringProvider.PAYMENT_CREATED + ": " + sucessfullPayment);
            if (convertedPayment != null) {
                buf.append(" (" + convertedPayment.getCurrency().getCode());
                buf.append(" " + convertedPayment.getAmount() + ")");
            }

            if (sucessfullPayment != null)
                createdPayments++;
        }
        // Dump error list
        int errCount = errorMessages.size();
        if (errCount > 0) {
            System.out.println();
            System.out.println(PAStringProvider.FILE_IMPORTED);
            System.out.println(errCount + " " + PAStringProvider.ERRORS_FOUND + ":");
            for (Entry<Integer, String> entry : errorMessages.entrySet()) {
                String message =
                        String.format(PAStringProvider.LINE + ": %d, %s: %s", entry.getKey(),
                                PAStringProvider.ERROR_MESSAGE, entry.getValue());
                System.out.println(message);
            }
        }

        return createdPayments;
    }

    /**
     * Starts System.in reading. Creates payments, and sawing them to repositories,
     * or writes format error if payment reading fails.
     *
     * @throws Exception - PAExceptions are caught and represented as format errors,
     *                   others thrown
     */
    @Override
    public void runCommandLineInterface() throws IOException, PAException {
        PAStringProvider.printHelpExitProgram();
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        while (true) {
            System.out.println();
            System.out.println(PAStringProvider.INPUT_ORDER);
            String line = br.readLine();

            if (line == null) {
                continue;
            }

            // Quit condition
            if (line.toLowerCase().equals(PAStringProvider.QUIT.toLowerCase())) {
                runtimeUtil.stop(0);
            }

            // Help condition
            if (line.toLowerCase().equals(PAStringProvider.HELP.toLowerCase())) {
                PAStringProvider.printHelp();
                continue;
            }

            PAPayment sucessfullPayment;
            String errorMessage = "";
            try {
                sucessfullPayment = validateAndCreatePayment(line);
            } catch (NumberFormatException nfe) {
                errorMessage = PAStringProvider.INCORRECT_FORMAT_OF_AMOUNT_NUMBER + ":\n" + nfe.getMessage();
                continue;
            } catch (PAException pae) {
                errorMessage = String.format(PAStringProvider.ERROR_MESSAGE + ": %s", pae.getMessage());
                continue;
            } finally {
                if (!errorMessage.isEmpty()) {
                    errorMessage = String.format(PAStringProvider.COULD_NOT_PARSE_PAYMENT_FROM + " '%s'!\n" + errorMessage, line);
                    System.out.println(errorMessage);
                    PAStringProvider.printHelpPaymentInsertCommandLine();
                }
            }
            PAPayment convertedPayment = paCurencyConverterService
                    .getConvertedPayment(sucessfullPayment, paCurrencyDatabase.getCurrencyByName(PAStringProvider.USD));
            StringBuilder buf = new StringBuilder(PAStringProvider.PAYMENT_CREATED + ": " + sucessfullPayment);
            if (convertedPayment != null) {
                buf.append(" (" + convertedPayment.getCurrency().getCode());
                buf.append(" " + convertedPayment.getAmount() + ")");
            }
            System.out.println(buf);
        }
    }

    /**
     * Validates format and creates PAPayment object from string if format is:
     * 3 capital letters, space, amount(may include minus for payments with negative balance impact)
     * e.g.:
     * USD 250
     * HKD -320
     *
     * @param line
     * @return PAPayment object if format was ok
     * @throws PAException           - input string parsing/format error exception,
     * @throws NumberFormatException -
     */
    private PAPayment validateAndCreatePayment(String line) throws PAException {
        if (line == null || line.isEmpty()) {
            throw new WrongArgumentException(PAStringProvider.NULL_OR_EMPTY_STRING_PASSED_TO_METHOD);
        }
        List<String> items = Arrays.asList(line.split(" "));

        // PAPayment and currency constructors validates input format and emptiness,
        // also throws relevant exceptions itself, just check list size:
        if (items.size() != 2) {
            String message = PAStringProvider.WRONG_COUNT_OF_INPUT_PARAMETERS_CANNOT_BE_PARSED_TO_CURRENCY_AND_VALUE;
            throw new WrongArgumentException(message);
        }

        PACurrency currency = paCurrencyDatabase.getCurrencyByName(items.get(0));

        // Add also USD rate fot currency:
        String usdCode = PAStringProvider.USD;
        if (!currency.getCode().equals(usdCode)) {
            paCurrencyRateDatabase.getDummyRate(paCurrencyDatabase.getCurrencyByName(usdCode), currency);
        }


        int amount = Integer.parseInt(items.get(1));
        PAPayment payment = new PAPayment(currency, amount);

        // Save payment to repositories
        paymentDatabaseRepository.addPayment(payment);

        return payment;
    }

}
