package cz.vaclavhanton.paymentsaccumulator.services;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;

public interface PACurencyConverterService {
    /**
     * Returns temporary(not saved to repositories) payment object, that holds amount recalculated
     * to target currency using rate from repositories
     *
     * @param p
     * @param targetCurrency
     * @return
     * @throws WrongArgumentException
     */
    PAPayment getConvertedPayment(PAPayment p, PACurrency targetCurrency)
            throws WrongArgumentException;
}
