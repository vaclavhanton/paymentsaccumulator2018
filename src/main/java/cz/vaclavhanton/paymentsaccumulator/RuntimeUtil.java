package cz.vaclavhanton.paymentsaccumulator;

import cz.vaclavhanton.paymentsaccumulator.services.PAPaymentsAccumulatorService;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.io.FileReader;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
@Component
@ComponentScan(basePackages = {"cz.vaclavhanton.paymentsaccumulator", "cz.vaclavhanton.paymentsaccumulator.services", "cz.vaclavhanton.paymentsaccumulator.schedulers", "cz.vaclavhanton.paymentsaccumulator.repositories"})
@EnableScheduling
public class RuntimeUtil implements CommandLineRunner {


    @Autowired
    @Qualifier("paymentsAccumulator")
    PAPaymentsAccumulatorService paymentsAccumulator;

    @Override
    public void run(String... args) throws Exception {

        if (args.length > 0) {

            // Write help and exit the program
            if (args[0].equals(PAStringProvider.HELP.toLowerCase())) {
                PAStringProvider.printHelp();
                stop(0);
            }

            // Try load from file - argument is expected to be a file name
            paymentsAccumulator.loadPaymentsFromFile(new FileReader(args[0]));
        }

        // Start start reading from System.in
        paymentsAccumulator.runCommandLineInterface();
    }

    /**
     * Use for full stop of the program.
     *
     * @param exitStatus
     */
    public void stop(int exitStatus) {
        System.out.println(PAStringProvider.GOOD_BYE);
        System.exit(exitStatus);
    }
}
