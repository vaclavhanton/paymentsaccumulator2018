package cz.vaclavhanton.paymentsaccumulator.utils;

/**
 * Created by vaclav.hanton on 14.6.2016.
 */
public class PAStringProvider {

    public final static String INCORRECT_FORMAT_OF_AMOUNT_NUMBER = "Incorrect format of amount number";
    public final static String COULD_NOT_PARSE_PAYMENT_FROM = "Could not parse payment from";
    public final static String EXPECTED_FORMAT_EXAMPLE = "Expected format example: 'CZK 500'";
    public final static String CANNOT_PARSE_CURRENCY_CODE = "Cannot parse currency code";
    public final static String ERROR_MESSAGE = "Error message";
    public final static String NULL_FILE_READER_PASSED = "Null fileReader passed to loadPaymentFromFile()!";
    public final static String PAYMENT_CREATED = "PAPayment created";
    public final static String FILE_IMPORTED = "File imported!";
    public final static String ERRORS_FOUND = "errors found";
    public final static String LINE = "Line";
    public final static String INPUT_ORDER = "Insert payment:";
    public final static String QUIT = "quit";
    public final static String USD = "USD";
    public final static String WRONG_COUNT_OF_INPUT_PARAMETERS_CANNOT_BE_PARSED_TO_CURRENCY_AND_VALUE = "Wrong count of input parameters - cannot be parsed to currency and value!";
    public final static String NULL_OR_EMPTY_STRING_PASSED_TO_METHOD = "Null or empty string passed to method";
    public final static String EMPTY_ARGUMENTS_FOR_CONVERTING_PAYMENT = "Empty arguments for converting payment";
    public final static String ACTUAL_BALANCE = "Actual balance";
    public final static String CANNOT_CREATE_RATE_EQUAL_CURRENCIES = "Cannot make create between for equal currencies";
    public final static String CURRENCY_NULL = "Currency is null";
    public final static String GOOD_BYE = "Good bye!";

    public final static String HELP = "help";
    public final static String HELP_HEADER = "### Help ###";
    public final static String HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_1 = "* After 'Insert payment' order type each payment in following format:";
    public final static String HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_2 = "* 3 capital chars, space, amount(negative of positive), hit enter";
    public final static String HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_3 = "* E.g.: AUS 50; USD -230;";
    public final static String HELP_PAYMENT_INSERT_FILE_INSTRUCTION_1 = "* You can specify list of payments in same format also in file.";
    public final static String HELP_PAYMENT_INSERT_FILE_INSTRUCTION_2 = "* To load from file run program with one argument - filename.";
    public final static String HELP_PAYMENT_EXIT_PROGRAM = "* Type 'quit' to exit Payments accumulator";

    public static void printHelp() {
        System.out.println(HELP_HEADER);
        printHelpPaymentInsertCommandLine();
        printHelpPaymentInsertFile();
        printHelpExitProgram();
    }

    public static void printHelpPaymentInsertCommandLine() {
        System.out.println(HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_1);
        System.out.println(HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_2);
        System.out.println(HELP_PAYMENT_INSERT_COMMAND_LINE_INSTRUCTION_3);
    }

    public static void printHelpPaymentInsertFile() {
        System.out.println(HELP_PAYMENT_INSERT_FILE_INSTRUCTION_1);
        System.out.println(HELP_PAYMENT_INSERT_FILE_INSTRUCTION_2);
    }

    public static void printHelpExitProgram() {
        System.out.println(HELP_PAYMENT_EXIT_PROGRAM);
    }

}

