package cz.vaclavhanton.paymentsaccumulator.schedulers;

import cz.vaclavhanton.paymentsaccumulator.repositories.PAPaymentDatabaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * This validator would be used for recalculation of balance, if needed.
 * Such validation would be beneficial if repositories are filled from multiple sources.
 * <p>
 * It is Scheduled for run every 5 seconds independently.
 */
@Configuration
public class PABalanceValidator {
    @Autowired
    @Qualifier("paymentDatabaseRepository")
    PAPaymentDatabaseRepository paymentDatabaseRepository;

    @Scheduled(fixedDelay = 5000)
    private void validateBalance() {
        if (!paymentDatabaseRepository.validateActualBalance()) {
            paymentDatabaseRepository.recalculateActualBalance();
        }
    }
}
