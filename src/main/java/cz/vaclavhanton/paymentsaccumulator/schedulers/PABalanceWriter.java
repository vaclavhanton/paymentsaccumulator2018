package cz.vaclavhanton.paymentsaccumulator.schedulers;

import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.repositories.PAPaymentDatabaseRepository;
import cz.vaclavhanton.paymentsaccumulator.utils.PAStringProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.DecimalFormat;
import java.util.Map;

@Configuration
public class PABalanceWriter {

    @Autowired
    @Qualifier("paymentDatabaseRepository")
    PAPaymentDatabaseRepository paymentDatabaseRepository;

    @Scheduled(fixedDelay = 36000)
    private void writeBalance() {

        Map<PACurrency, Double> actualBalance = paymentDatabaseRepository.getActualBalance();

        // Check if there is something to print:
        if (actualBalance != null && !actualBalance.isEmpty()) {
            StringBuilder buff = new StringBuilder("\n");
            buff.append(PAStringProvider.ACTUAL_BALANCE);
            buff.append(":\n");
            for (Map.Entry<PACurrency, Double> entry : actualBalance.entrySet()) {
                buff.append(
                        String.format("%s %s%n", entry.getKey().getCode(), new DecimalFormat("#").format(entry.getValue())));
            }
            buff.append("\n------");
            System.out.println(buff);

        }
    }
}
