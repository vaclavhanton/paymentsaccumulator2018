package cz.vaclavhanton.paymentsaccumulator.exceptions;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
public class PAException extends Exception {
    public PAException(String message) {
        super(message);
    }
}
