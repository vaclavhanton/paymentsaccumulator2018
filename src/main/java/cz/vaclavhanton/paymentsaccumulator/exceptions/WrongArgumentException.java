package cz.vaclavhanton.paymentsaccumulator.exceptions;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
public class WrongArgumentException extends PAException {

    public WrongArgumentException(String message) {
        super(message);
    }
}
