package cz.vaclavhanton.paymentsaccumulator.exceptions;

/**
 * Created by vaclav.hanton on 8.6.2016.
 */
public class CurrencyNotParsableException extends PAException {
    
    public CurrencyNotParsableException(String message) {
        super(message);
    }
}
