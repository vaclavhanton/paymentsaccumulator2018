package cz.vaclavhanton.paymentsaccumulator.exceptions;

/**
 * Created by vaclav.hanton on 9.6.2016.
 */
public class WrongPaymentFileException extends PAException {
    public WrongPaymentFileException(String message) {
        super(message);
    }
}
