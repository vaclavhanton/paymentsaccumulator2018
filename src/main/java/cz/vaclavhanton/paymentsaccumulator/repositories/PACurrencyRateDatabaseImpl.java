package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrencyRate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by vaclav.hanton on 13.6.2016.
 */
@Repository("currencyRateDatabase")
public class PACurrencyRateDatabaseImpl implements PACurrencyRateDatabase {

    private static Set<PACurrencyRate> rates = Collections.synchronizedSet(new HashSet<PACurrencyRate>());

    private static boolean addRateToSet(PACurrencyRate rate) {
        return rates.add(rate);
    }

    private PACurrencyRate createRandomRate(PACurrency a, PACurrency b)
            throws WrongArgumentException {

        double randomRate = new Random().nextDouble() * (1.5 - 0.5) + 0.5;

        return new PACurrencyRate(a, b, randomRate);
    }

    /**
     * If there is no rate object for converting a to b, it creates it with RANDOM rate
     * between 0.5 - 1.5 and add to the repositories. If such rate exists, it is returned.
     *
     * @param a
     * @param b
     * @return new or found rate object
     * @throws WrongArgumentException - same currencies, etc.
     */
    @Override
    public PACurrencyRate getDummyRate(PACurrency a, PACurrency b)
            throws WrongArgumentException {

        // No rate for equal currencies
        if (a.equals(b)) {
            return null;
        }

        for (PACurrencyRate rate : PACurrencyRateDatabaseImpl.rates) {
            if (rate.getA().equals(a) && rate.getB().equals(b)) {
                return rate;
            }
        }
        // rate not found
        PACurrencyRate newRate = createRandomRate(a, b);
        PACurrencyRateDatabaseImpl.addRateToSet(newRate);

        return newRate;
    }

}
