package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.exceptions.CurrencyNotParsableException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Database-like service providing storage services for PACurrency objects.
 * <p/>
 * Created by vaclav.hanton on 11.6.2016.
 */
@Repository("currencyDatabaseRepository")
public class PACurrencyDatabaseRepositoryImpl implements PACurrencyDatabaseRepository {

    private static Set<PACurrency> currencies = Collections.synchronizedSet(new HashSet<PACurrency>());

    /**
     * This methods returns Currency for given code. If currency for the code is not in the repositories,
     * method creates new one, saves it, and returns it. Code is validated in Currency constructor.
     *
     * @param code - currency code, 3 upprecase alpha chars
     * @return new or found Currency for given code
     * @throws CurrencyNotParsableException
     */
    @Override
    public PACurrency getCurrencyByName(String code) throws CurrencyNotParsableException {
        for (PACurrency currency : currencies) {
            if (currency.getCode().equals(code)) {
                return currency;
            }
        }

        PACurrency newCurrency = new PACurrency(code);

        addToSet(newCurrency);

        return newCurrency;
    }

    /**
     * @param currency
     * @return true if this set did not already contained the element
     */
    private boolean addToSet(PACurrency currency) {
        return currencies.add(currency);
    }
}
