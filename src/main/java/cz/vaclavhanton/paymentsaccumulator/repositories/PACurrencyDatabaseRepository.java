package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.exceptions.CurrencyNotParsableException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;

public interface PACurrencyDatabaseRepository {

    /**
     * This methods returns Currency for given code. If currency for the code is not in the repositories,
     * method creates new one, saves it, and returns it. Code is validated in Currency constructor.
     *
     * @param code - currency code, 3 upprecase alpha chars
     * @return new or found Currency for given code
     * @throws CurrencyNotParsableException
     */
    PACurrency getCurrencyByName(String code) throws CurrencyNotParsableException;
}
