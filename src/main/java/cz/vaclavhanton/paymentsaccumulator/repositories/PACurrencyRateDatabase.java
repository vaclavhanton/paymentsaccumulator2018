package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.exceptions.WrongArgumentException;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PACurrencyRate;

public interface PACurrencyRateDatabase {
    /**
     * If there is no rate object for converting a to b, it creates it with RANDOM rate
     * between 0.5 - 1.5 and add to the repositories. If such rate exists, it is returned.
     *
     * @param a
     * @param b
     * @return new or found rate object
     * @throws WrongArgumentException - same currencies, etc.
     */
    PACurrencyRate getDummyRate(PACurrency a, PACurrency b)
            throws WrongArgumentException;
}
