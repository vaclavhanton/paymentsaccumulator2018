package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;

import java.util.Map;

public interface PAPaymentDatabaseRepository {
    /**
     * Ads payment to repositories, and also to the actual balance
     *
     * @param payment
     * @return true if added successfully
     */
    boolean addPayment(PAPayment payment);

    /**
     * For longer uptime, actual balance can be validated with actual payments list
     *
     * @return boolean - is actual balance allign with payments list?
     */
    boolean validateActualBalance();

    /**
     * Recalculates actual balance from payments list if needed.
     * E.g.: validation fails, repositories change etc.
     */
    void recalculateActualBalance();

    /**
     * Returns actual balance map
     *
     * @return - actual amount in form of Map<currency, amount>
     */
    Map<PACurrency, Double> getActualBalance();
}
