package cz.vaclavhanton.paymentsaccumulator.repositories;

import cz.vaclavhanton.paymentsaccumulator.model.PACurrency;
import cz.vaclavhanton.paymentsaccumulator.model.PAPayment;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Database-like service providing storage services for PAPayment objects.
 * For performance reasons also holds needed balanceData.
 * <p/>
 * Created by vaclav.hanton on 11.6.2016.
 */
@Repository("paymentDatabaseRepository")
public class PAPaymentDatabaseRepositoryImpl implements PAPaymentDatabaseRepository {

    private static List<PAPayment> payments = Collections.synchronizedList(new ArrayList<PAPayment>());

    private static Map<PACurrency, Double> actualBalance = new HashMap<>();

    /**
     * Ads payment to repositories, and also to the actual balance
     *
     * @param payment
     * @return true if added successfully
     */
    @Override
    public boolean addPayment(PAPayment payment) {

        boolean added = PAPaymentDatabaseRepositoryImpl.payments.add(payment);

        if (added) {
            addToBalance(payment, PAPaymentDatabaseRepositoryImpl.actualBalance);
        }

        return added;
    }

    /**
     * For longer uptime, actual balance can be validated with actual payments list
     *
     * @return boolean - is actual balance align with payments list?
     */
    @Override
    public boolean validateActualBalance() {

        Map<PACurrency, Double> calculatedBalance = calculateActualBalance();

        if (calculatedBalance.size() != PAPaymentDatabaseRepositoryImpl.actualBalance.size()) {
            return false;
        }

        for (Map.Entry<PACurrency, Double> calculatedEntry : calculatedBalance.entrySet()) {
            PACurrency calculatedCurrency = calculatedEntry.getKey();
            Double calculatedAmount = calculatedEntry.getValue();

            // Find amount for same currency
            Double actualAmount = PAPaymentDatabaseRepositoryImpl.actualBalance.get(calculatedCurrency);

            // If no, or different amount for currency - validation false
            if (actualAmount == null || calculatedAmount != actualAmount) {
                return false;
            }
        }


        // Same balance members count with same amount for each denomination here.
        // Validation true
        return true;
    }

    /**
     * Recalculates actual balance from payments list if needed.
     * E.g.: validation fails, repositories change etc.
     */
    @Override
    public void recalculateActualBalance() {
        PAPaymentDatabaseRepositoryImpl.actualBalance.clear();

        PAPaymentDatabaseRepositoryImpl.actualBalance.putAll(calculateActualBalance());
    }

    /**
     * Returns actual balance map
     *
     * @return - actual amount in form of Map<currency, amount>
     */
    @Override
    public Map<PACurrency, Double> getActualBalance() {
        return PAPaymentDatabaseRepositoryImpl.actualBalance;
    }

    /**
     * Adds payment to given balance map. 0 balances are removed, or not added.
     *
     * @param payment
     * @param balance
     */
    private void addToBalance(PAPayment payment, Map<PACurrency, Double> balance) {
        PACurrency paymentCurrency = payment.getCurrency();
        Double foundAmount = balance.get(paymentCurrency);

        if (foundAmount == null) {
            if (payment.getAmount() != 0) {
                balance.put(paymentCurrency, payment.getAmount());
            }
        } else {
            double sum = foundAmount + payment.getAmount();
            if (sum == 0) {
                balance.remove(paymentCurrency);
            } else {
                balance.put(paymentCurrency, sum);
            }
        }

    }

    /**
     * Calculates balance from payments list
     *
     * @return calculated balance map
     */
    private Map<PACurrency, Double> calculateActualBalance() {
        Map<PACurrency, Double> calculatedBalance = new HashMap<>();

        for (PAPayment payment : payments) {
            addToBalance(payment, calculatedBalance);
        }

        return calculatedBalance;
    }

}
