# Payments Accumulator #
This is repository done to fulfill task given by BSC.

* This is simple java program, that reads payments input from file and from System.in, stores payments, summarizes actual balance, and creates random rate of currencies to USD
* Version: 1.0
* Comments and assumptions regarding to requirements and design can be found in **PaymentsAccumulatorNotes.md** file in the root of the project

## Set up ##
* From project root run **gradlew install**, and locate runnable *PaymentsAccumulator* (also .bat available) file in *build\install\paymentsaccumulator\bin*. Build process should download dependecies, prepare build and running scripts

## Run (Windows) ##
### PaymentsAccumulator ###
To run deployed program, to see help:

* *build\install\paymentsaccumulator\bin\paymentaccumulator.bat help*
* You can get help by typing *help* to console during payments inserting also.

To run deployed program, for read only from System.in:

* *build\install\paymentsaccumulator\bin\paymentaccumulator.bat*

To run deployed program, with given payments file, run in the project root:

* *build\install\paymentsaccumulator\bin\paymentaccumulator.bat data\payments.list*

### JUnit ###
To run one available JUnit test in project root:
* *gradlew test*
* Then look for html results in *build\reports\*
* To ensure tests are recompiled, force its run, and for verbose output run: *gradlew cleanTest test -i*

